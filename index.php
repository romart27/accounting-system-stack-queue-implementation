<!--
//Created by : Kid Romart Medinate
//Started Date : Sunday 30, 2017; 5:12 PM
//Deadline Date : Tuestday 1, 2017
//Data Struture on WebApp
-->
<html>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="description" content="Web Based - LIFO Accounting System"/>
  <meta property="og:title" content="Web Based - LIFO Accounting System">
  <meta property="og:image" content="../images/icon/logo.png">
  <meta property="og:description" content="Web Based - LIFO Accounting System">
  <!--Stylesheet-->
  <link href="fonts/iconfont/material-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="css/lato.css"/>
  <link rel="stylesheet" href="css/app.css"/>
  <title>Web Based - Accounting System</title>
 </head>
<body>
  <div class="developer">Kid Romart A. Mediante - Data Structure</div>
  <header>
    <div class="title">Accounting System</div>
  </header>
  <nav>
    <ul>
      <li  class="transaction">
        <i class="material-icons">account_balance_wallet</i>
        <span>New Transaction</span>
      </li>
      <li  class="reports">
        <i class="material-icons">assignment_turned_in</i>
        <span>Reports</span>
      </li>
      <li  class="history">
        <i class="material-icons">history</i>
        <span>History</span>
        <div class="nav-pin day"></div>
      </li>
      <li  class="manage">
        <i class="material-icons">work</i>
        <span>Manage</span>
      </li>
    </ul>
  </nav>
  <main>
    <div class="welcome">
    </div>
    <section id="transaction">
      <div class="panel-box">
        <div class="container-box" id="purchase">
          <div class="header">
            <i class="material-icons">add_shopping_cart</i>
          </div>
          <div class="c-b-title">Purchase</div>
        </div>
        <div class="container-box" id="sell">
          <div class="header">
            <i class="material-icons">store</i>
          </div>
          <div class="c-b-title">Sell</span>
        </div>
      </div>
    </section>
    <section id="reports">
      <div class="panel-reports">
        <div class="left-corner">
          <canvas id="myChartGain" width="300" height="300"></canvas>.
          <canvas id="myChartAssets" width="300" height="300"></canvas>
        </div>
        <div class="right-corner">
          <table class="table-chart">
            <tr>
              <th>Day No.</th>
              <th>Method</th>
              <th>Price</th>
              <th>Units No</th>
              <th>Units Remaining</th>
              <th>Total Gain Amount</th>
              <th>Assets Sold</th>
              <th>Total Assets</th>
            </tr>
          </table>
        </div>
        <div class="right-corner">
          <div class="card-breakdown">
            <div class="card-title">
              <span><b>Transaction Breakdown</b></span>
            </div>
            <div class="card-data">

            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="history">
      <div class="h-main-content"></div>
    </section>
    <section id="manage">
      <div class="btn-next smt-clear">
        <span>Clear All Data</span>
      </div>
    </section>
  </main>
  <div class="dialogs">
    <div class="middle">
      <div class="d-m-center purchase-share">
        <div class="d-title">
          <i class="material-icons">input</i>
          <span>Purchase Share</span>
          <div class="cls-d">
            <i class="material-icons">close</i>
          </div>
        </div>
        <div class="d-content">
          <div class="p-p-s1 flex">
            <div class="d-label">
            <span>Enter Price Per Share</span>
            </div>
            <input type="number" min="1" class="pps in-number" />
          </div>
          <div class="p-p-s2 flex">
            <div class="d-label">
            <span>Enter No. of Units</span>
            </div>
            <input type="number" min="1" class="pps-units in-number" />
          </div>
          <span style="visibility:hidden;font-size: 25px;">Finish</span>
          <div class="pps btn-next">
            <span>Finish</span>
          </div>
        </div>
      </div>

      <div class="d-m-center sell-ask">
        <div class="d-title">
          <i class="material-icons">input</i>
          <span>What Computation do you want to do? </span>
          <div class="cls-d">
            <i class="material-icons">close</i>
          </div>
        </div>
        <div class="btn-lifo btn-blue">
          <span>LIFO</span>
        </div>
        <div class="btn-fifo btn-blue">
          <span>FIFO</span>
        </div>
        </div>
      </div>

      <div class="middle">
        <div class="d-m-center sell-share-lifo">
          <div class="d-title">
            <i class="material-icons">input</i>
            <span>Selling Share - LIFO</span>
            <div class="cls-d">
              <i class="material-icons">close</i>
            </div>
          </div>
          <div class="d-content">
            <div class="s-p-s1 flex">
              <div class="d-label">
              <span>Enter Price Per Share</span>
              </div>
              <input type="number" class="lifo in-number" />
            </div>
            <div class="s-p-s2 flex">
              <div class="d-label">
              <span>Enter No. of Units max(<i class="value"></i>)</span>
              </div>
              <input type="number" min="1" class="lifo-units in-number" />
            </div>
            <span style="visibility:hidden;font-size: 25px;">Finish</span>
            <div class="btn-next smt-lifo">
              <span>Finish</span>
            </div>
          </div>
        </div>
      </div>
      <div class="middle">
        <div class="d-m-center sell-share-fifo">
          <div class="d-title">
            <i class="material-icons">input</i>
            <span>Selling Share - FIFO</span>
            <div class="cls-d">
              <i class="material-icons">close</i>
            </div>
          </div>
          <div class="d-content">
            <div class="s-p-s1 flex">
              <div class="d-label">
              <span>Enter Price Per Share</span>
              </div>
              <input type="number" class="fifo in-number" />
            </div>
            <div class="s-p-s2 flex">
              <div class="d-label">
              <span>Enter No. of Units max(<i class="value"></i>)</span>
              </div>
              <input type="number" min="1" class="fifo-units in-number" />
            </div>
            <span style="visibility:hidden;font-size: 25px;">Finish</span>
            <div class="btn-next smt-fifo">
              <span>Finish</span>
            </div>
          </div>
        </div>
      </div>
      <div class="middle">
        <div class="d-m-center alert-data">
          <div class="d-title">
            <i class="material-icons">input</i>
            <span>Warning - Clear All Data</span>
            <div class="cls-d">
              <i class="material-icons">close</i>
            </div>
          </div>
          <div class="d-content">
            <div class="s-p-s1 flex">
              <div class="d-label">
              <span>Are you sure you wanted to clear all data?</span>
              </div>
            </div>
            <span style="visibility:hidden;font-size: 25px;">Finish</span>
            <div class="btn-next smt-yes">
              <span>Yes</span>
            </div>
            <div class="btn-next smt-no">
              <span>No</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="breadcrumbs">
    <div class="b-top success">
      <div class="b-label">
        <div class="d-label b-msg"></div>
      </div>
    </div>
  </div>
  <script src="js/jquery.min.js"></script>
  <script src="dist/Chart.bundle.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
